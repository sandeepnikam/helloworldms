package nikam.sandeep.helloworldms.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/", produces = "application/json")
public class HelloWorld {


    @GetMapping(path = "hello")
    public ResponseEntity hello(){
        ResponseEntity<String> response = new ResponseEntity<String>("HelloWorld", HttpStatus.OK);
        return response;
    }
}
