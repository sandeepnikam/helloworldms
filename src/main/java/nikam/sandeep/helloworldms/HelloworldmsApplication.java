package nikam.sandeep.helloworldms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloworldmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloworldmsApplication.class, args);
	}

}
